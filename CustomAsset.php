<?php
/**
 * Created by PhpStorm.
 * User: vov
 * Date: 9/11/17
 * Time: 10:56 PM
 */

namespace mihaildev\elfinder;


use yii\web\AssetBundle;

class CustomAsset extends AssetBundle
{
    public $css = array(
        'custom.css'
    );

    public $depends = array(
        'yii\jui\JuiAsset',
        'mihaildev\elfinder\Assets',
    );


    public function init()
    {
        $this->sourcePath = __DIR__."/assets/custom";
        parent::init();
    }
}