<?php
/**
 * Created by PhpStorm.
 * User: vov
 * Date: 9/11/17
 * Time: 10:56 PM
 */

namespace mihaildev\elfinder;


use yii\web\AssetBundle;

class MaterialAsset extends AssetBundle
{
    public $css = array(
        'css/theme.css'
    );

    public $depends = array(
        'yii\jui\JuiAsset',
        'mihaildev\elfinder\Assets',
    );


    public function init()
    {
        $this->sourcePath = __DIR__."/assets/material";
        parent::init();
    }
}